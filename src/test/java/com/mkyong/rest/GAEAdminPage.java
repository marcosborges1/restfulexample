package com.mkyong.rest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GAEAdminPage {

	private WebDriver driver;
	private boolean isLocal;

	public GAEAdminPage(WebDriver driver, boolean isLocal) {
		this.driver = driver;
		this.isLocal = isLocal;
	}
	public void visit() {
		String url = (isLocal) ? "http://localhost:8080/_ah/admin/":"https://console.cloud.google.com/storage/browser/upteste-1247.appspot.com/";
		driver.get(url);
	}

	public GAEDataStoreViewPage dataStoreViewer() {
		driver.findElement(By.id("datastore_viewer_link")).click();
		return new GAEDataStoreViewPage(driver);
	}

}
