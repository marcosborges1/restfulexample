package com.mkyong.rest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class GAEDataStoreViewPage {
	
	private WebDriver driver;

	public GAEDataStoreViewPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void selectBucket(String nameBucket) {
		WebElement cbInput = driver.findElement(By.id("kind_input"));
		new Select(cbInput).selectByVisibleText(nameBucket);
		cbInput.submit();
	}
	
	public boolean checkFileInList(String nameFile) {
		return driver.getPageSource().contains(nameFile);
	}
}
