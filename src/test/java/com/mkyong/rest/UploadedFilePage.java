package com.mkyong.rest;

import org.openqa.selenium.WebDriver;

public class UploadedFilePage {

	private WebDriver driver;

	public UploadedFilePage(WebDriver driver) {
		this.driver = driver;
	}

	public boolean checkUploaedFile(String path) {
		return driver.getPageSource().contains(path);
	}

}
