package com.mkyong.rest;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

public class UploadFileServiceTest {
	private FirefoxDriver driver;
	UploadFileBasePage uploadPage;
	GAEAdminPage gaeAdminPage;

	// Upload Files Information
	boolean isLocalServer;
	private String nameBucket;
	private String nameDir = "/Users/marcosborges/Desktop/";
	private String nameFile = "generate.txt";

	@Before
	public void initialize() {
		this.driver = new FirefoxDriver();
		isLocalServer = true;
		this.uploadPage = new UploadFileBasePage(driver, isLocalServer);
		this.gaeAdminPage = new GAEAdminPage(driver, isLocalServer);
		this.nameBucket = (isLocalServer) ? "_ah_FakeCloudStorage__MyBucket" : "upteste-1247.appspot.com";
	}

	@Test
	public void checkUploadFile() {
		uploadPage.visit();
		assertTrue(uploadPage.sendFile(nameDir + nameFile).checkUploaedFile(nameFile));
	}

	@Test
	public void checkUploadedFileAdmin() {
		gaeAdminPage.visit();
		GAEDataStoreViewPage dataStoreViewPage = gaeAdminPage.dataStoreViewer();
		if (isLocalServer) {
			dataStoreViewPage.selectBucket(nameBucket);
		}
		dataStoreViewPage.checkFileInList(nameFile);	
	}

	@After
	public void close() {
		driver.close();
	}
}
