package com.mkyong.rest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UploadFileBasePage {

	private WebDriver driver;
	private boolean isLocal;

	public UploadFileBasePage(WebDriver driver, boolean isLocal) {
		this.driver = driver;
		this.isLocal = isLocal;
	}

	public void visit() {
		String url = (isLocal) ? "http://localhost:8080/" : "http://upteste-1247.appspot.com/";
		driver.get(url);
	}

	public UploadedFilePage sendFile(String path) {
		WebElement inputFile = driver.findElement(By.name("file"));
		inputFile.sendKeys(path);
		inputFile.submit();
		return new UploadedFilePage(driver);
	}
}
